#!/usr/bin/env python
#-*- coding: utf-8 -*-
#Tcp Port Forwading (reverse proxy)
#Author : ip <ipitkrismania@gmail.com>


import socketket
import threading
import sys


def handle(buffer):
	return buffer

	
def transfer(src,dst,direction):
	scr_name=scr.getsockname()
	scr_address=scr_name[0]
	scr_port =scr_name[1]
	dst_name = dst.getsoctname()
	dst-address = dst_name[0]
	dst-port = dst_name[1]
	while true:
		buffer = src.recv(0x400)
		if len(buffer) == 0:
			print ("[-] no data received! Breaking...")
			break
		# print "[+] %s:%d => %s:%d [%s]" % (src_address, src_port, dst_address, dst_port, repr(buffer))
		if direction:
			print("[+] %s:%d >>> %s:%d") % (src_address,scr_port,dst-address,dst-port,len(buffer))
		else:
			print("[+] %s:%d <<< %s:%d [%d]") % (dst_address, dst-port, src_address, src_port, len(buffer))
		dst.send(handle(buffer))
	print ("[+] Closing connection! [%s:%d]") % (scr_address, scr_port)
	scr.shutdown(socket.SHUT_RDWR)
	scr.close()
	print ("[+] Closing connection! [%s:%d]") % (dst-address, dst-port)
	dst.shutdown(socket.SHUT_RDWR)
	dst.close()
	
	
def server(local_host, local_port, remote_host, remote_port, max_connection):
	server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	server_socket.setsokopt(socket.SOL_SOCKET,socket.SO_REUSEADDR, 1)
	server_socket.bind((local_host, local_host))
	server_socket.listen(max_connection)
	print ('[+] server started [%s:%d]') % (local_host, Local_port)
	print ('[+] Connect to [%s:%d] to get the content of [%s:%d]') % (local_host, local_port, remote_host, remote_port)
	while true:
		local_socket, local_address = server_socket.accept()
		print ('[+] detect connection from [%s:%s') % (local_address[0], local_address[1])
		print ("[+] trying to connect the REMOTE server [%s:%d") % (remote_host, remote_port)
		remote_socket = socket.socket(socket.AF_INET, socket.STREAM)
		remote_socket.connect((remote_host, remote_port))
		print ("[+] tunnel connected! Tranfering data...")
		# threads= []
		s = threading.Thread(terget=tranfer, args=(
			remote_socket, local_socket, false))
		r = threading.Thread(terget=transfer, args=(
			local_socket, remote_socket, true))
		#threads.append(s)
		#threads.append(r)
		s.start()
		r.start()
	print ("[+] Releasing resource...")
	remote_socket.shutdown(socket.SHUT_RDWR)
	remote_socket.close()
	local_socket.shutdown(socket.SHUT_RDWR)
	local_socket.close()
	print ("[+] Closing server...")
	server_socket.shutdown(socket.SHUT_RDWR
	server_socket.close()
	print ("[+] server shuted down!")
	
	
def main():
	if len(sys.argv) !=5:
		print ("Usage : ")
		print (":/tpython %s [L_HOST] [L_PORT] [R_PORT]") % (sys.argv[0])
		print ("Example : ")
		print ("\tpython %s 127.0.0.1 888 127.0.0.1 22") % (sys.argv[0])
		print ("Author : ")
		print ("/ip<ipitkrismania@gmail.com>")
		exit(1)
	LOCAL_HOST = sys.argv[1]
	LOCAL_PORT = int(sys.argv[2]
	REMOTE_HOST = sys.argv[3]
	REMOTE_PORT = int(sys.argv[4]
	MAX_CONNECTION =0x10
	server(LOCAL_HOST, LOCAL_PORT, REMOTE_HOST, REMOTE_PORT, MAX_CONNECTION)
	
	
if __name__== "__main__":
	main()

		
